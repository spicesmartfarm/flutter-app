import 'package:ProjectSPICE/bottom_navigation_bar_controller.dart';
import 'package:ProjectSPICE/my_classes/datasource.dart';

import 'package:flutter_login/flutter_login.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'my_classes/constants.dart';

class LoginScreen extends StatelessWidget {
  Duration get loginTime => Duration(milliseconds: 2250);

  Future<String> _registerUser(LoginData data) {
    return Future.delayed(loginTime).then((_) {
      launch(Constants.registerUrl);
      return null;
    });
  }

  Future<String> _resetPassword(String data) {
    return Future.delayed(loginTime).then((_) {
      launch(Constants.registerUrl);
      return null;
    });
  }

  Future<String> _authUser(LoginData data) {
    print("Authenticating...");
    print("username: ${data.name}");
    print("password: ${data.password}");
    return DataSource().login(data.name, data.password).then((result) {
      print("$result");
      if (result == "success") return null;
      return "$result";
    });
  }

  static final FormFieldValidator<String> myEmailValidator = (value) {
    if (value.isEmpty) {
      return 'Invalid email!';
    }
    return null;
  };

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.amber[600],
      child: FlutterLogin(
        title: Constants.appName,
        logo: "images/spice.png",
        onLogin: _authUser,
        onSignup: _registerUser,
        onRecoverPassword: _resetPassword, //_recoverPassword,
        onSubmitAnimationCompleted: () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) =>
                BottomNavigationBarController(title: Constants.appName),
          ));
        },
        emailValidator: myEmailValidator,
        messages: LoginMessages(
          usernameHint: 'Username',
          passwordHint: 'Password',
          confirmPasswordHint: 'Confirm Password',
          loginButton: 'LOG IN',
          signupButton: 'REGISTER',
          forgotPasswordButton: 'Forgot Your Password?',
          recoverPasswordButton: 'SEND EMAIL',
          goBackButton: 'Back',
          confirmPasswordError: 'Passwords do not match!',
          recoverPasswordDescription: 'Send a recovery email?',
          recoverPasswordSuccess: 'Password rescued successfully',
        ),
      ),
    );
  }
}
