import 'package:flutter/material.dart';

import 'models/model_plant_farm.dart';
import 'my_classes/datasource.dart';

class DashboardWidget extends StatefulWidget {
  const DashboardWidget({Key key}) : super(key: key);

  @override
  _DashboardWidgetState createState() => _DashboardWidgetState();
}

class _DashboardWidgetState extends State<DashboardWidget> {
  final String _text = "Welcome Back, FARMER!";

  @override
  void initState() {
    fetchFarmList();
    super.initState();
  }

  Future<String> createHarvestPlantDialog(BuildContext context, String farmName) {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Text("Harvest Farm"),
              content: Text("Are you sure you want to harvest now?"),
              actions: <Widget>[
                MaterialButton(
                  elevation: 5.0,
                  child: Text("Harvest"),
                  onPressed: () {
                    DataSource()
                        .harvestFarm(farmName)
                        .then((result) {
                      Navigator.of(context).pop(result);
                    });
                  },
                ),
                MaterialButton(
                  elevation: 5.0,
                  child: Text("Cancel"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
        });
  }

  Widget showFarmPlantsList(List<FarmPlants> farms) {
    if(farms.length == 0) {
      return Center(
        child: Container(
          height: 20,
          width: 20,
          child: CircularProgressIndicator(),
        ),
      );
    }
    return ListView.builder(
      itemCount: farms.length,
      itemBuilder: (context, index) {
        return Card(
          child: Container(
            child: Center(
              child: ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage('icons/sprout.png'),
                ),
                title: Text(
                  "${farms[index].farmName}",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(
                    "${farms[index].plantType} \nPlanted on: ${farms[index].datePlanted} \nHarvest on: ${farms[index].dateHarvest}"),
                onTap: () {
                  createHarvestPlantDialog(context, farms[index].farmName).then((value) {
                    if(value == "success")
                      value = "Harvested Farm Successfully!";
                      fetchFarmList();
                    SnackBar snackBar = SnackBar(
                      content: Text(value),
                    );
                    Scaffold.of(context).showSnackBar(snackBar);
                  });
                },
              ),
            ),
          ),
        );
      },
    );
  }

  Future<String> createAddPlantDialog(BuildContext context) {
    var _plantTypes = ["Lettuce1", "Lettuce2"];
    var _currentPlantSelected = _plantTypes[0];
    var _farmList = [
      "UPD-EEEI HVL Farm",
      "UPLB-IPB Farm",
      "UPD-EEEI Microlab Farm"
    ];
    var _selectedFarm = _farmList[0];

    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Text("Plant a Seedling"),
              content: Container(
                height: 130,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text("Select Plant:  "),
                        DropdownButton<String>(
                          items: _plantTypes.map((String item) {
                            return DropdownMenuItem<String>(
                              value: item,
                              child: Text(item),
                            );
                          }).toList(),
                          onChanged: (String itemSel) {
                            setState(() {
                              _currentPlantSelected = itemSel;
                            });
                          },
                          value: _currentPlantSelected,
                        ),
                      ],
                    ),
                    /*Row(
                      children: <Widget>[
                        Text("Plant Name:  "),
                        Expanded(child:
                        TextField(
                          controller: plantNameController,
                        ),
                        ),
                      ],
                    ),*/
                    SizedBox(height: 10),
                    Text("Select Farm:  "),
                    DropdownButton<String>(
                      items: _farmList.map((String item) {
                        return DropdownMenuItem<String>(
                          value: item,
                          child: Text(item),
                        );
                      }).toList(),
                      onChanged: (String itemSel) {
                        setState(() {
                          _selectedFarm = itemSel;
                        });
                      },
                      value: _selectedFarm,
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                MaterialButton(
                  elevation: 5.0,
                  child: Text("Plant"),
                  onPressed: () {
                    DataSource()
                        .plantSeedling(_currentPlantSelected, _selectedFarm)
                        .then((result) {
                      Navigator.of(context).pop(result);
                    });
                  },
                ),
                MaterialButton(
                  elevation: 5.0,
                  child: Text("Cancel"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
        });
  }

  List<FarmPlants> farmsWithPlants = [];

  fetchFarmList(){
    DataSource().fetchFarmWithPlants().then((result) {
      setState(() {
        farmsWithPlants = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            _text,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Image(
            height: 150,
            image: AssetImage("images/smartfarm_bg.jpg"),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: Container(
              color: Colors.teal[50],
              height: 400,
              padding: const EdgeInsets.all(10.0),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Planted Crops",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      height: 340,
                      child: showFarmPlantsList(farmsWithPlants),
                    ),
                    RaisedButton(
                      onPressed: () {
                        createAddPlantDialog(context).then((value) {
                          if(value == "success")
                            value = "Planted Seedling Successfully!";
                            fetchFarmList();
                          SnackBar snackBar = SnackBar(
                            content: Text(value),
                          );
                          Scaffold.of(context).showSnackBar(snackBar);
                        });
                      },
                      child: const Text(
                        "PLANT SEEDLINGS",
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                        ),
                      ),
                      color: Colors.teal,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
