import 'dart:convert';

List<PlantSeedling> plantSeedlingFromJson(String str) => List<PlantSeedling>.from(json.decode(str).map((x) => PlantSeedling.fromJson(x)));

String plantSeedlingToJson(List<PlantSeedling> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PlantSeedling {
  PlantSeedling({
    this.status,
  });

  String status;

  factory PlantSeedling.fromJson(Map<String, dynamic> json) => PlantSeedling(
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
  };
}
