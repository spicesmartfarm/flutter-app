// To parse this JSON data, do
//
//     final rackDetails = rackDetailsFromJson(jsonString);

import 'dart:convert';

RackDetails rackDetailsFromJson(String str) =>
    RackDetails.fromJson(json.decode(str));

String rackDetailsToJson(RackDetails data) => json.encode(data.toJson());

class RackDetails {
  String rackId;
  List<RackInfo> rackInfo;
  List<SensorInfo> sensorInfo;

  RackDetails({
    this.rackId,
    this.rackInfo,
    this.sensorInfo,
  });

  factory RackDetails.fromJson(Map<String, dynamic> json) => RackDetails(
        rackId: json["rack_id"],
        rackInfo: List<RackInfo>.from(
            json["rack_info"].map((x) => RackInfo.fromJson(x))),
        sensorInfo: List<SensorInfo>.from(
            json["sensor_info"].map((x) => SensorInfo.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "rack_id": rackId,
        "rack_info": List<dynamic>.from(rackInfo.map((x) => x.toJson())),
        "sensor_info": List<dynamic>.from(sensorInfo.map((x) => x.toJson())),
      };
}

class RackInfo {
  String nodeId;
  String layerId;
  List<int> sensorTypes;

  RackInfo({
    this.nodeId,
    this.layerId,
    this.sensorTypes,
  });

  factory RackInfo.fromJson(Map<String, dynamic> json) => RackInfo(
        nodeId: json["node_id"],
        layerId: json["layer_id"],
        sensorTypes: List<int>.from(json["sensor_types"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "node_id": nodeId,
        "layer_id": layerId,
        "sensor_types": List<dynamic>.from(sensorTypes.map((x) => x)),
      };
}

class SensorInfo {
  String sensorGroup;
  //SensorGroup sensorGroup;
  String sensorMType;
  String sensorName;
  String sensorScale;
  String sensorUnit;

  SensorInfo({
    this.sensorGroup,
    this.sensorMType,
    this.sensorName,
    this.sensorScale,
    this.sensorUnit,
  });

  factory SensorInfo.fromJson(Map<String, dynamic> json) => SensorInfo(
        sensorGroup:
            json["sensorGroup"], //sensorGroupValues.map[json["sensorGroup"]],
        sensorMType: json["sensorMType"],
        sensorName: json["sensorName"],
        sensorScale: json["sensorScale"],
        sensorUnit: json["sensorUnit"],
      );

  Map<String, dynamic> toJson() => {
        "sensorGroup": sensorGroup, //sensorGroupValues.reverse[sensorGroup],
        "sensorMType": sensorMType,
        "sensorName": sensorName,
        "sensorScale": sensorScale,
        "sensorUnit": sensorUnit,
      };
}

enum SensorGroup { MACRO, WATER, MICRO }

final sensorGroupValues = EnumValues({
  "Macro": SensorGroup.MACRO,
  "Micro": SensorGroup.MICRO,
  "Water": SensorGroup.WATER
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
