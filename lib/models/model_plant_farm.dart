import 'dart:convert';

List<FarmPlants> farmPlantsFromJson(String str) => List<FarmPlants>.from(json.decode(str).map((x) => FarmPlants.fromJson(x)));
String farmPlantsToJson(List<FarmPlants> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FarmPlants {
  FarmPlants({
    this.farmId,
    this.farmName,
    this.rackCount,
    this.plantType,
    this.datePlanted,
    this.dateHarvest,
  });

  int farmId;
  String farmName;
  int rackCount;
  String plantType;
  DateTime datePlanted;
  DateTime dateHarvest;

  factory FarmPlants.fromJson(Map<String, dynamic> json) => FarmPlants(
    farmId: json["farmID"],
    farmName: json["farmName"],
    rackCount: json["rackCount"],
    plantType: json["plantType"],
    datePlanted: DateTime.parse(json["datePlanted"]),
    dateHarvest: DateTime.parse(json["dateHarvest"]),
  );

  Map<String, dynamic> toJson() => {
    "farmID": farmId,
    "farmName": farmName,
    "rackCount": rackCount,
    "plantType": plantType,
    "datePlanted": datePlanted.toIso8601String(),
    "dateHarvest": dateHarvest.toIso8601String(),
  };
}