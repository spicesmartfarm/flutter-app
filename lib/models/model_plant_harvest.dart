import 'dart:convert';

List<HarvestPlants> harvestPlantsFromJson(String str) => List<HarvestPlants>.from(json.decode(str).map((x) => HarvestPlants.fromJson(x)));

String harvestPlantsToJson(List<HarvestPlants> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HarvestPlants {
  HarvestPlants({
    this.status,
  });

  String status;

  factory HarvestPlants.fromJson(Map<String, dynamic> json) => HarvestPlants(
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
  };
}
