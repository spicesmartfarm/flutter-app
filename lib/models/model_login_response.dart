import 'dart:convert';

LoginResponse loginResponseFromJson(String str) =>
    LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  String status;
  String idToken;
  String accessToken;
  int expiresIn;
  String refreshToken;

  LoginResponse({
    this.status,
    this.idToken,
    this.accessToken,
    this.expiresIn,
    this.refreshToken,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        status: json["status"],
        idToken: json["id_token"],
        accessToken: json["access_token"],
        expiresIn: json["expires_in"],
        refreshToken: json["refresh_token"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "id_token": idToken,
        "access_token": accessToken,
        "expires_in": expiresIn,
        "refresh_token": refreshToken,
      };
}
