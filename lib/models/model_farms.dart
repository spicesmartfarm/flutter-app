// To parse this JSON data, do
//
//     final farms = farmsFromJson(jsonString);

import 'dart:convert';

Farms farmsFromJson(String str) => Farms.fromJson(json.decode(str));

String farmsToJson(Farms data) => json.encode(data.toJson());

class Farms {
  int farmCount;
  List<Farm> farmList;

  Farms({
    this.farmCount,
    this.farmList,
  });

  factory Farms.fromJson(Map<String, dynamic> json) => Farms(
        farmCount: json["farm_count"],
        farmList: List<Farm>.from(json["farms"].map((x) => Farm.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "farm_count": farmCount,
        "farms": List<dynamic>.from(farmList.map((x) => x.toJson())),
      };
}

class Farm {
  String farmId;
  FarmInfo farmInfo;
  List<String> farmNodes;
  int farmNodecount;

  Farm({
    this.farmId,
    this.farmInfo,
    this.farmNodes,
    this.farmNodecount,
  });

  factory Farm.fromJson(Map<String, dynamic> json) => Farm(
        farmId: json["farm_id"],
        farmInfo: FarmInfo.fromJson(json["farm_info"]),
        farmNodes: List<String>.from(json["farm_nodes"].map((x) => x)),
        farmNodecount: json["farm_nodecount"],
      );

  Map<String, dynamic> toJson() => {
        "farm_id": farmId,
        "farm_info": farmInfo.toJson(),
        "farm_nodes": List<dynamic>.from(farmNodes.map((x) => x)),
        "farm_nodecount": farmNodecount,
      };
}

class FarmInfo {
  String farmName;
  String farmLocation;

  FarmInfo({
    this.farmName,
    this.farmLocation,
  });

  factory FarmInfo.fromJson(Map<String, dynamic> json) => FarmInfo(
        farmName: json["farmName"],
        farmLocation: json["farmLocation"],
      );

  Map<String, dynamic> toJson() => {
        "farmName": farmName,
        "farmLocation": farmLocation,
      };
}
