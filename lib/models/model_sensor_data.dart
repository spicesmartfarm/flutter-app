// To parse this JSON data, do
//
//     final sensorData = sensorDataFromJson(jsonString);

import 'dart:convert';

SensorData sensorDataFromJson(String str) =>
    SensorData.fromJson(json.decode(str));

String sensorDataToJson(SensorData data) => json.encode(data.toJson());

class SensorData {
  List<Item> items;
  int count;
  List<LastEvaluatedKey> lastEvaluatedKey;
  List<String> prevPages;

  SensorData({
    this.items,
    this.count,
    this.lastEvaluatedKey,
    this.prevPages,
  });

  factory SensorData.fromJson(Map<String, dynamic> json) => SensorData(
        items: List<Item>.from(json["Items"].map((x) => Item.fromJson(x))),
        count: json["Count"],
        lastEvaluatedKey: List<LastEvaluatedKey>.from(
            json["LastEvaluatedKey"].map((x) => LastEvaluatedKey.fromJson(x))),
        prevPages: List<String>.from(json["PrevPages"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "Items": List<dynamic>.from(items.map((x) => x.toJson())),
        "Count": count,
        "LastEvaluatedKey":
            List<dynamic>.from(lastEvaluatedKey.map((x) => x.toJson())),
        "PrevPages": List<dynamic>.from(prevPages.map((x) => x)),
      };
}

class Item {
  double mValue;
  DateTime timeSent;

  Item({
    this.mValue,
    this.timeSent,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        mValue: json["mvalue"].toDouble(),
        timeSent: DateTime.parse(json["timesent"]),
      );

  Map<String, dynamic> toJson() => {
        "mvalue": mValue,
        "timesent": timeSent.toIso8601String(),
      };
}

class LastEvaluatedKey {
  int sensorSetId;
  DateTime timeSent;

  LastEvaluatedKey({
    this.sensorSetId,
    this.timeSent,
  });

  factory LastEvaluatedKey.fromJson(Map<String, dynamic> json) =>
      LastEvaluatedKey(
        sensorSetId: json["sensorsetid"],
        timeSent: DateTime.parse(json["timesent"]),
      );

  Map<String, dynamic> toJson() => {
        "sensorsetid": sensorSetId,
        "timesent": timeSent.toIso8601String(),
      };
}
