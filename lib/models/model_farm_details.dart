// To parse this JSON data, do
//
//     final farmDetails = farmDetailsFromJson(jsonString);

import 'dart:convert';

FarmDetails farmDetailsFromJson(String str) =>
    FarmDetails.fromJson(json.decode(str));

String farmDetailsToJson(FarmDetails data) => json.encode(data.toJson());

class FarmDetails {
  String farmId;
  String farmName;
  String farmLocation;
  int farmNodeCount;
  List<FarmNode> farmNodes;

  FarmDetails({
    this.farmId,
    this.farmName,
    this.farmLocation,
    this.farmNodeCount,
    this.farmNodes,
  });

  factory FarmDetails.fromJson(Map<String, dynamic> json) => FarmDetails(
        farmId: json["farm_id"],
        farmName: json["farm_name"],
        farmLocation: json["farm_location"],
        farmNodeCount: json["farm_nodecount"],
        farmNodes: List<FarmNode>.from(
            json["farm_nodes"].map((x) => FarmNode.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "farm_id": farmId,
        "farm_name": farmName,
        "farm_location": farmLocation,
        "farm_nodecount": farmNodeCount,
        "farm_nodes": List<dynamic>.from(farmNodes.map((x) => x.toJson())),
      };
}

class FarmNode {
  String nodeId;
  String rackId;
  String layerId;

  FarmNode({
    this.nodeId,
    this.rackId,
    this.layerId,
  });

  factory FarmNode.fromJson(Map<String, dynamic> json) => FarmNode(
        nodeId: json["node_id"],
        rackId: json["rack_id"],
        layerId: json["layer_id"],
      );

  Map<String, dynamic> toJson() => {
        "node_id": nodeId,
        "rack_id": rackId,
        "layer_id": layerId,
      };
}
