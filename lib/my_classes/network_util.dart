import 'dart:async';
import 'package:http/http.dart' as http;

class NetworkUtil {
  // next three lines makes this class a Singleton
  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  Future<dynamic> get(String url) {
    print("Sending GET request to server...");
    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      print("status code: " + statusCode.toString());
      if (response == null || statusCode != 200) {
        print("response: " + res.toString());
        return Future.error(
          "Couldn't fetch data from server.",
          StackTrace.fromString("in network_util.dart's get function"),
        );
      } else
        return res;
    });
  }

  Future<dynamic> put(String url, {Map headers, body, encoding}) {
    print("Sending PUT request to server...");
    return http
        .put(url, headers: headers, body: body /*, encoding: encoding*/)
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      print("status code: " + statusCode.toString());
      if (response == null || statusCode < 200 || statusCode > 400) {
        print("response: " + res.toString());
        return Future.error(
          "Couldn't connect to server.",
          StackTrace.fromString("in network_util.dart's put function"),
        );
      }
      return res;
    }, onError: (error) {
      print("put.onError: " + error);
    });
  }

  Future<dynamic> post(String url, {Map headers, body, encoding}) {
    print("Sending POST request to server...");
    return http
        .post(url, headers: headers, body: body /*, encoding: encoding*/)
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      print("status code: " + statusCode.toString());
      if (response == null || statusCode < 200 || statusCode > 400) {
        print("response: " + res.toString());
        return Future.error(
          "Couldn't connect to server.",
          StackTrace.fromString("in network_util.dart's put function"),
        );
      }
      return res;
    }, onError: (error) {
      print("post.onError: " + error);
    });
  }
}
