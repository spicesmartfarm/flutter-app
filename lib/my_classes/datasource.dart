import 'dart:async';
import 'dart:convert';
import 'dart:collection';

import 'package:ProjectSPICE/models/model_login_response.dart';
import 'package:ProjectSPICE/models/model_farms.dart';
import 'package:ProjectSPICE/models/model_farm_details.dart';
import 'package:ProjectSPICE/models/model_plant_farm.dart';
import 'package:ProjectSPICE/models/model_plant_harvest.dart';
import 'package:ProjectSPICE/models/model_rack_details.dart';
import 'package:ProjectSPICE/models/model_sensor_data.dart';

import 'package:ProjectSPICE/my_classes/constants.dart';
import 'package:ProjectSPICE/my_classes/network_util.dart';

class DataSource {
  NetworkUtil _netUtil = new NetworkUtil();
  static const BASE_URL = Constants.baseUrl;
  static const LOGIN_URL = Constants.loginUrl;
  static const REMOTE_ACCESS = Constants.remoteAcc;
  static const PLANTS_URL = Constants.plantURL;

  Future<String> login(String username, String password) {
    print("Creating Login Request...");

    Map<String, String> headers = new HashMap();
    //headers['Accept'] = 'application/json';
    headers['content-type'] = "application/json";

    return _netUtil
        .put(LOGIN_URL,
            headers: headers,
            body: json.encode({"username": username, "password": password}))
        .then((response) {
      final result = loginResponseFromJson(response);
      if (result.status != "success")
        return "Failed to Login. Please Try Again.";
      return result.status;
    }).catchError((error, stacktrace) {
      print("catchError: $error");
      return "$error";
    });
  }

  Future<Farms> fetchFarms(String farmer) {
    print("Creating Fetch Farms Request...");
    var url = "$BASE_URL/spicefarm/$farmer";

    return _netUtil.get(url).then((response) {
      final result = farmsFromJson(response);
      return result;
    }).catchError((error, stacktrace) {
      print("catchError: $error");
      throw Exception("$error");
    });
  }

  Future<FarmDetails> fetchFarmDetails(String farmerID, String farmID) {
    print("Creating Fetch Farm Details Request...");
    var url = "$BASE_URL/spicefarm/$farmerID/$farmID";

    return _netUtil.get(url).then((response) {
      final result = farmDetailsFromJson(response);
      return result;
    }).catchError((error, stacktrace) {
      print("catchError: $error");
      throw Exception("$error");
    });
  }

  Future<RackDetails> fetchRackDetails(
      String farmerID, String farmID, String rackID) {
    print("Creating Fetch Rack Details Request...");
    var url = "$BASE_URL/spicefarm/$farmerID/$farmID/$rackID";

    return _netUtil.get(url).then((response) {
      final result = rackDetailsFromJson(response);
      return result;
    }).catchError((error, stacktrace) {
      print("catchError: $error");
      throw Exception("$error");
    });
  }

  Future<SensorData> fetchSensorData(
      String nodeID, String mType, DateTime end) {
    var timeEnd = end.toIso8601String();
    var timeStart = end.subtract(Duration(days: 30)).toIso8601String();

    print("Creating Fetch Sensor Data Request...");
    var url =
        "$BASE_URL/spicesensor/$nodeID/$mType?timestart=$timeStart&timeend=$timeEnd";
    print("url: $url");

    return _netUtil.get(url).then((response) {
      print(response.toString());
      final result = sensorDataFromJson(response);
      return result;
    }).catchError((error, stacktrace) {
      print("catchError: $error");
      throw Exception("$error");
    });
  }

  Future<List<FarmPlants>> fetchFarmWithPlants() {
    print("Creating Fetch Farms with Plants Request...");
    var url = "$PLANTS_URL/getHarvest";
    print("url: $url");

    return _netUtil.get(url).then((response) {
      print(response.toString());
      final result = farmPlantsFromJson(response);
      return result;
    }).catchError((error, stacktrace) {
      print("catchError: $error");
      throw Exception("$error");
    });
  }

  Future<String> harvestFarm(String farmName) {
    print("Creating Harvest Farm Request...");

    Map<String, String> headers = new HashMap();
    //headers['Accept'] = 'application/json';
    headers['content-type'] = "application/json";

    var url = "$PLANTS_URL/harvest";
    print("url: $url");

    return _netUtil
        .post(url,
        headers: headers,
        body: json.encode({"farmName": farmName}))
        .then((response) {
      final result = harvestPlantsFromJson(response);
      if (result[0].status != "success")
        return "Failed to Harvest. Please Try Again.";
      return result[0].status;
    }).catchError((error, stacktrace) {
      print("catchError: $error");
      return "$error";
    });
  }

  Future<String> plantSeedling(String plantType, String farmName) {
    print("Creating Plant Seedling Request...");

    DateTime start = DateTime.now();
    var timeStart = start.toIso8601String();
    var timeEnd = start.add(Duration(days: 30)).toIso8601String();

    Map<String, String> headers = new HashMap();
    //headers['Accept'] = 'application/json';
    headers['content-type'] = "application/json";

    var url = "$PLANTS_URL/submit";
    print("url: $url");

    return _netUtil
        .post(url,
        headers: headers,
        body: json.encode({"plantType": plantType,
          "datePlanted": timeStart,
          "dateHarvest": timeEnd,
          "farmName": farmName}))
        .then((response) {
      final result = harvestPlantsFromJson(response);
      if (result[0].status != "success")
        return "Failed to Harvest. Please Try Again.";
      return result[0].status;
    }).catchError((error, stacktrace) {
      print("catchError: $error");
      return "$error";
    });
  }

}
