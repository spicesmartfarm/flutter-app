class Constants {
  static const String appName = "Project SPICE";
  static const String logoTag = "ph.org.spiceprogram.logo";
  static const String titleTag = "ph.org.spiceprogram.title";
  static const String baseUrl =
      "https://kk6nxk1b0k.execute-api.us-east-1.amazonaws.com/test1";
  static const String loginUrl =
      "https://asf5eo9n3i.execute-api.us-east-1.amazonaws.com/test1/auth";
  static const String registerUrl =
      "https://spiceprogram.auth.us-east-1.amazoncognito.com/signup?response_type=token&client_id=fp74nh1us3s2bq1khoth6kk8q&redirect_uri=https://spiceprogram.auth.us-east-1.amazoncognito.com";
  static const String remoteAcc =
      "https://pl0uzy5pmi.execute-api.us-east-1.amazonaws.com/test1";
  static const String plantURL = "http://spice-program.org";
}
