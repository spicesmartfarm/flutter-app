import 'package:ProjectSPICE/models/model_farms.dart';
import 'package:ProjectSPICE/models/model_rack_details.dart';
import 'package:ProjectSPICE/widget_sensordata.dart';
import 'package:flutter/material.dart';

class RackWidget extends StatelessWidget {
  const RackWidget(this.rackName, this.farmerInfo, this.rackDetails, {Key key})
      : super(key: key);

  final String rackName;
  final Farm farmerInfo;
  final RackDetails rackDetails;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${farmerInfo.farmInfo.farmName}: $rackName"),
        //backgroundColor: Colors.teal,
      ),
      body: Column(
        children: <Widget>[
          Container(
            constraints: BoxConstraints.expand(
              height: Theme.of(context).textTheme.headline4.fontSize + 5.0,
            ),
            padding: const EdgeInsets.all(8.0),
            alignment: Alignment.centerLeft,
            child: Text(
              "Rack Nodes",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) =>
                  RackItem(rackDetails.rackInfo[index], rackDetails.sensorInfo),
              itemCount: rackDetails.rackInfo.length,
            ),
          ),
          //LayerWidget(rackDetails),
        ],
      ),
    );
  }
}

class RackItem extends StatelessWidget {
  const RackItem(this.rackInfo, this.sensorInfo);
  final RackInfo rackInfo;
  final List<SensorInfo> sensorInfo;

  Widget _buildRackItem(int sensorType) {
    List<bool> isSelected = List(3);
    isSelected[0] = true;
    isSelected[0] = false;
    isSelected[0] = false;

    int sensorIndex = sensorInfo.indexWhere(
        (sensor) => sensor.sensorMType == sensorType.toInt().toString());

    return Container(
      margin: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
      width: double.infinity - 200,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "${sensorInfo[sensorIndex].sensorGroup} - MType: ${sensorInfo[sensorIndex].sensorMType}",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Text(
                "${sensorInfo[sensorIndex].sensorName} (${sensorInfo[sensorIndex].sensorUnit})",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Divider(),
              Container(
                width: double.infinity,
                child: //Text("PLOT SENSOR DATA"),
                    SensorItem(
                        rackInfo.nodeId,
                        sensorType,
                        sensorInfo[sensorIndex].sensorScale,
                        sensorInfo[sensorIndex].sensorUnit),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildRack(RackInfo rackInfo) {
    if (rackInfo.sensorTypes.isEmpty)
      return ListTile(
        title: Text("Node #${rackInfo.nodeId} (${rackInfo.layerId})"),
        subtitle: Text("Sensors: No Sensors"),
      );
    else
      return ExpansionTile(
        key: PageStorageKey<RackInfo>(rackInfo),
        title: Text(
            "Node #${rackInfo.nodeId} (${rackInfo.layerId})\nSensors: ${rackInfo.sensorTypes}"),
        children: rackInfo.sensorTypes.map(_buildRackItem).toList(),
      );
  }

  @override
  Widget build(BuildContext context) {
    return _buildRack(rackInfo);
  }
}

/*
class SensorItem extends StatefulWidget {
  const SensorItem(
      this.nodeId, this.sensorType, this.sensorScale, this.sensorUnit,
      {Key key})
      : super(key: key);

  final String nodeId;
  final double sensorType;
  final String sensorScale;
  final String sensorUnit;

  _SensorItemState createState() => _SensorItemState();
}

class _SensorItemState extends State<SensorItem> {
  List<bool> isSelected;
  int currentSelected = 0;
  int days = 1;
  DateTime end;
  DateTime start;
  String dateRange = "";
  String lastUpdateDate = "";
  List<Item> currentSensorData;
  int chartState = 0;

  @override
  void initState() {
    end = DateTime.now();
    start = DateTime.now();
    isSelected = [true, false, false];
    currentSensorData = [];
    super.initState();
  }

  String getDateRange() {
    switch (days) {
      case 30:
        start = end.subtract(Duration(days: 30));
        dateRange = start.toString().split(" ")[0] +
            " to " +
            end.toString().split(" ")[0];
        break;
      case 7:
        start = end.subtract(Duration(days: 7));
        dateRange = start.toString().split(" ")[0] +
            " to " +
            end.toString().split(" ")[0];
        break;
      default:
        start = end.subtract(Duration(days: 1));
        dateRange = end.toString().split(" ")[0];
    }
    return dateRange;
  }

  String getLastUpdateDate() {
    return lastUpdateDate;
  }

  List<Item> getSensorData() {
    return currentSensorData;
  }

  Widget getBody() {
    if (chartState == 1) {
      return Container(
        margin: EdgeInsets.only(top: 5, bottom: 5),
        width: 20,
        height: 20,
        child: CircularProgressIndicator(),
      );
    } else if (chartState == 2) {
      return Container(
        width: double.infinity,
        height: 250,
        child: LineChartWidget(
          getSensorData(),
          widget.sensorScale,
          widget.sensorUnit,
          start,
        ),
      );
    } else if (chartState == 3) {
      return Container(
        margin: EdgeInsets.only(top: 5, bottom: 5),
        width: 20,
        height: double.infinity,
        child: Center(
          child: Text("No Data Found."),
        ),
      );
    } else {
      return RaisedButton(
        onPressed: () {
          chartState = 1;
          DataSource()
              .fetchSensorData(
                  widget.nodeId, widget.sensorType.toInt().toString(), end)
              .then((result) {
            if (result.items != null && result.items.length != 0) {
              chartState = 2;
              currentSensorData = result.items;
              lastUpdateDate = currentSensorData[0].timeSent.toIso8601String();
            } else {
              chartState = 3;
            }
          });
        },
        child: const Text(
          "Load Chart Data",
          style: TextStyle(
            fontSize: 12,
            color: Colors.white,
          ),
        ),
        color: Colors.teal,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ToggleButtons(
          children: <Widget>[
            Text("Day"),
            Text("Week"),
            Text("Month"),
          ],
          borderRadius: BorderRadius.circular(8),
          constraints: BoxConstraints.tight(Size(80.0, 25.0)),
          onPressed: (int index) {
            setState(
              () {
                if (index != currentSelected) {
                  currentSelected = index;
                  if (index == 0)
                    days = 1;
                  else if (index == 1)
                    days = 7;
                  else
                    days = 30;
                  isSelected = [false, false, false];
                  isSelected[index] = true;
                }
              },
            );
          },
          isSelected: isSelected,
        ),
        Container(
          margin: EdgeInsets.only(top: 5, bottom: 5),
          child: Text(
            "Date:  ${getDateRange()}",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        getBody(),
        Text("Last Updated On:  ${getLastUpdateDate()}"),
        /*FutureBuilder<SensorData>(
          future: DataSource()
              .fetchSensorData(
                  widget.nodeId, widget.sensorType.toInt().toString(), end)
              .then((result) {
            return result;
          }),
          //fetchSensorData(widget.nodeId, widget.sensorType.toInt().toString(), end),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Item> sensorData = snapshot.data.items;
              if (snapshot.data.items == null ||
                  snapshot.data.items.length == 0)
                return Text("No Data Received.");
              lastUpdateDate = sensorData[0].timeSent.toIso8601String();
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 250,
                    child: LineChartWidget(
                      sensorData,
                      widget.sensorScale,
                      widget.sensorUnit,
                      start,
                    ),
                  ),
                  Text("Last Updated On:  ${getLastUpdateDate()}"),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("Error: ${snapshot.error}");
            }
            return Center(
              child: Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
                width: 20,
                height: 20,
                child: CircularProgressIndicator(),
              ),
            );
          },
        ),*/
      ],
    );
  }
}*/
