import 'package:ProjectSPICE/models/model_sensor_data.dart';
import 'package:ProjectSPICE/my_classes/datasource.dart';
//import 'package:ProjectSPICE/widget_chart_line.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class SensorItem extends StatefulWidget {
  const SensorItem(
      this.nodeId, this.sensorType, this.sensorScale, this.sensorUnit,
      {Key key})
      : super(key: key);

  final String nodeId;
  final int sensorType;
  final String sensorScale;
  final String sensorUnit;

  _SensorItemState createState() => _SensorItemState();
}

class _SensorItemState extends State<SensorItem> {
  List<bool> isSelected;
  int currentSelected = 0;
  int days = 1;
  DateTime end;
  DateTime start;
  String dateRange = "";
  String lastUpdateDate = "";
  List<Item> currentSensorData;
  int chartState = 0;

  List<Color> gradientColors = [
    Colors.teal,
  ];

  double xValueShift = 0;
  double minGridX = 0;
  double maxGridX = 0;
  double minGridY = 0;
  double maxGridY = 0;

  double minValY = 0;
  double aveValY = 0;
  double maxValY = 0;

  @override
  void initState() {
    end = DateTime.now();
    start = DateTime.now();
    isSelected = [true, false, false];
    currentSensorData = [];
    super.initState();
  }

  String getDateRange() {
    switch (days) {
      case 30:
        start = end.subtract(Duration(days: 30));
        dateRange = start.toString().split(" ")[0] +
            " to " +
            end.toString().split(" ")[0];
        break;
      case 7:
        start = end.subtract(Duration(days: 7));
        dateRange = start.toString().split(" ")[0] +
            " to " +
            end.toString().split(" ")[0];
        break;
      default:
        start = end.subtract(Duration(days: 1));
        dateRange = end.toString().split(" ")[0];
    }
    return dateRange;
  }

  String getLastUpdateDate() {
    return lastUpdateDate;
  }

  List<Item> getSensorData() {
    return currentSensorData;
  }

  List<FlSpot> getDataPoints() {
    List<FlSpot> data = [];
    List<FlSpot> secondData = [];
    List<FlSpot> finalData = [];
    maxValY = -1;
    minValY = -1;
    maxGridX = -1;
    minGridX = -1;
    currentSensorData.forEach((sensorData) {
      if (sensorData.timeSent.isAfter(start) ||
          sensorData.timeSent.isAtSameMomentAs(start)) {
        var xVal = sensorData.timeSent.millisecondsSinceEpoch.roundToDouble();
        var yVal = sensorData.mValue;
        data.add(FlSpot(xVal, yVal));
        if (minValY == -1 &&
            maxValY == -1 &&
            minGridX == -1 &&
            maxGridX == -1) {
          minValY = yVal;
          maxValY = yVal;
          minGridX = xVal;
          maxGridX = xVal;
        } else {
          if (yVal < minValY) minValY = yVal;
          if (yVal > maxValY) maxValY = yVal;
          if (xVal < minGridX) minGridX = xVal;
          if (xVal > maxGridX) maxGridX = xVal;
        }
      }
    });

    // shift X values by minGridX
    xValueShift = minGridX;
    minGridX = 0;
    maxGridX = (maxGridX - xValueShift) / 10000;

    // get Y average and shift xValues
    aveValY = 0;
    data.forEach((f) {
      aveValY += f.y / data.length;
      secondData.add(FlSpot((f.x - xValueShift) / 10000, f.y));
    });

    // get Y grid
    minGridY = minValY - (maxValY - minValY) * 0.5;
    maxGridY = maxValY + (maxValY - minValY) * 0.5;

    print("minValY: $minValY\t aveValY: $aveValY\t maxValY: $maxValY");
    print("minGridY: $minGridY\t maxGridY: $maxGridY");
    print(
        "minGridX: $minGridX\t maxGridX: $maxGridX\t xValueShift: $xValueShift");

    //make sure x axis is sorted.
    secondData.sort((a, b) => a.x.compareTo(b.x));

    double prevData = secondData[0].x;
    double increment = 6 * 60 * 60 * 1000 / 10000; // 6 hours
    finalData.add(secondData[0]);
    for (int i = 1; i < secondData.length; i++) {
      if (secondData[i].x > prevData + increment) {
        finalData.add(FlSpot(secondData[i].x, secondData[i].y));
        prevData = secondData[i].x;
      } else if (secondData[i].y == maxValY) {
        finalData.add(FlSpot(secondData[i].x, secondData[i].y));
      } else if (secondData[i].y == minValY) {
        finalData.add(FlSpot(secondData[i].x, secondData[i].y));
      }
    }
    print("got here");
    if (finalData[finalData.length - 1].x !=
        secondData[secondData.length - 1].x)
      finalData.add(secondData[secondData.length - 1]);
    print("got two here");
    finalData.forEach((f) {
      print(f.toString());
      print(f.x);
      print(f.y);
    });
    print(finalData.length);
    return finalData;
  }

  String getDateFromX(double value, bool newline) {
    var date = DateTime.fromMillisecondsSinceEpoch(
        (value * 10000 + xValueShift).toInt());
    String dateString = "${date.month}/${date.day}/${date.year}" +
        (newline ? "\n" : " ") +
        "${date.hour}:${date.minute}:${date.second}";
    return dateString;
  }

  List<Widget> getChildren() {
    var widgets = List<Widget>();
    if (chartState == 1) {
      print("got into if for chartstate $chartState");
      widgets.add(Container(
        margin: EdgeInsets.only(top: 5, bottom: 5),
        width: 20,
        height: 20,
        child: CircularProgressIndicator(),
      ));
    } else if (chartState == 2) {
      print("got into if for chartstate $chartState");
      widgets.add(
        ToggleButtons(
          children: <Widget>[
            Text("Day"),
            Text("Week"),
            Text("Month"),
          ],
          borderRadius: BorderRadius.circular(8),
          constraints: BoxConstraints.tight(Size(80.0, 25.0)),
          onPressed: (int index) {
            setState(
              () {
                if (index != currentSelected) {
                  currentSelected = index;
                  if (index == 0)
                    days = 1;
                  else if (index == 1)
                    days = 7;
                  else
                    days = 30;
                  isSelected = [false, false, false];
                  isSelected[index] = true;
                }
              },
            );
          },
          isSelected: isSelected,
        ),
      );
      widgets.add(
        Container(
          margin: EdgeInsets.only(top: 5, bottom: 5),
          child: Text(
            "Date:  ${getDateRange()}",
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      );
      /*widgets.add(
        Container(
          width: double.infinity,
          height: 250,
          child: LineChartWidget(
            getSensorData(),
            widget.sensorScale,
            widget.sensorUnit,
            start,
          ),
        ),
      );*/
      widgets.add(
        Container(
          margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
          width: double.infinity,
          height: 250,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(10),
              ),
              color: Colors.grey[100]),
          child: Padding(
            padding: const EdgeInsets.only(
                right: 10, left: 8.0, top: 15, bottom: 10),
            child: LineChart(
              LineChartData(
                gridData: FlGridData(
                  show: true,
                  drawHorizontalLine: true,
                  getDrawingVerticalLine: (value) {
                    return FlLine(
                      color: Color(0xFFBDBDBD),
                      strokeWidth: 0.5,
                    );
                  },
                  getDrawingHorizontalLine: (value) {
                    return FlLine(
                      color: Color(0xFFBDBDBD),
                      strokeWidth: 0.5,
                    );
                  },
                ),
                lineTouchData: LineTouchData(
                    getTouchedSpotIndicator:
                        (LineChartBarData barData, List<int> spotIndexes) {
                      return spotIndexes.map((spotIndex) {
                        //final FlSpot spot = barData.spots[spotIndex];
                        //if (spot.x == 0 || spot.x == 6) return null;
                        return TouchedSpotIndicatorData(
                          FlLine(color: Colors.teal, strokeWidth: 2),
                          FlDotData(),
                          //FlDotData(dotSize: 5, dotColor: Colors.teal),
                        );
                      }).toList();
                    },
                    touchTooltipData: LineTouchTooltipData(
                        tooltipBgColor: Colors.teal,
                        getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
                          return touchedBarSpots.map((barSpot) {
                            final flSpot = barSpot;
                            //if (flSpot.x == 0 || flSpot.x == 6) return null;
                            return LineTooltipItem(
                              "${flSpot.y} ${widget.sensorUnit}\n${getDateFromX(flSpot.x, false)}",
                              const TextStyle(color: Colors.white),
                            );
                          }).toList();
                        })),
                titlesData: FlTitlesData(
                  show: true,
                  bottomTitles: SideTitles(
                    showTitles: true,
                    reservedSize: 22,
                    margin: 8,
                    interval: (maxGridX - minGridX) / 4, //8640,
                    textStyle: TextStyle(
                      color: Colors.black45,
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                    getTitles: (value) {
                      return getDateFromX(value, true);
                      //var date = DateTime.fromMillisecondsSinceEpoch((value * 10000 + xValueShift).toInt());
                      //return "${date.month}/${date.day}/${date.year}\n${date.hour}:${date.minute}:${date.second}";
                    },
                  ),
                  leftTitles: SideTitles(
                    showTitles: true,
                    reservedSize: 35,
                    margin: 8,
                    interval: (maxGridY - minGridY) / 6,
                    textStyle: TextStyle(
                      color: Colors.black45,
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                    getTitles: (value) {
                      return ((value * 10000).toInt().toDouble() / 10000)
                          .toString();
                    },
                  ),
                ),
                borderData: FlBorderData(
                  show: true,
                  border: Border.all(
                    color: Colors.black45,
                    width: 1,
                  ),
                ),
                lineBarsData: [
                  LineChartBarData(
                    spots: getDataPoints(),
                    isCurved: true,
                    colors: gradientColors,
                    barWidth: 3,
                    isStrokeCapRound: true,
                    // dotData: FlDotData(show: false, dotSize: 4, dotColor: Colors.teal, ),
                    belowBarData: BarAreaData(
                      show: true,
                      colors: gradientColors
                          .map((color) => color.withOpacity(0.3))
                          .toList(),
                    ),
                  ),
                ],
                /*minX: minGridX,
                maxX: maxGridX,
                minY: minGridY,
                maxY: maxGridY,
                extraLinesData: ExtraLinesData(
                  horizontalLines: [
                    HorizontalLine(
                      y: minValY,
                      color: Colors.red.withOpacity(0.7),
                      strokeWidth: 3,
                    ),
                    HorizontalLine(
                      y: aveValY,
                      color: Colors.red.withOpacity(0.7),
                      strokeWidth: 3,
                    ),
                    HorizontalLine(
                      y: maxValY,
                      color: Colors.red.withOpacity(0.7),
                      strokeWidth: 3,
                    ),
                  ],
                ),*/
              ),
            ),
          ),
        ),
      );
      widgets.add(
        Text("Last Updated On:  ${getLastUpdateDate()}"),
      );
    } else if (chartState == 3) {
      print("got into if for chartstate $chartState");
      widgets.add(Container(
        margin: EdgeInsets.only(top: 5, bottom: 5),
        width: double.infinity,
        height: 20,
        child: Center(
          child: Text("No Data Found."),
        ),
      ));
    }
    widgets.add(RaisedButton(
      onPressed: () {
        setState(() {
          chartState = 1;
          DataSource()
              .fetchSensorData(
                  widget.nodeId, widget.sensorType.toInt().toString(), end)
              .then((result) {
            print("Hello Then Start Result. $chartState");
            if (result.items != null && result.items.length != 0) {
              currentSensorData = result.items;
              lastUpdateDate = currentSensorData[0].timeSent.toIso8601String();
              setState(() {
                chartState = 2;
              });
            } else {
              setState(() {
                chartState = 3;
              });
            }
            print("Hello Then End Result. $chartState");
          });
        });
      },
      child: const Text(
        "Load Chart Data",
        style: TextStyle(
          fontSize: 12,
          color: Colors.white,
        ),
      ),
      color: Colors.teal,
    ));

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: getChildren(),
    );
  }
}
