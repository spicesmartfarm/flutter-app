import 'package:ProjectSPICE/models/model_farms.dart';
import 'package:ProjectSPICE/models/model_rack_details.dart';
import 'package:ProjectSPICE/models/model_farm_details.dart';
import 'package:ProjectSPICE/my_classes/datasource.dart';
import 'package:ProjectSPICE/widget_rack.dart';
import 'package:flutter/material.dart';

ListView showRackList(
    List<String> farmRacks, String farmerID, String farmID, Farm farmInfo) {
  return ListView.builder(
    itemCount: farmRacks.length,
    //physics: NeverScrollableScrollPhysics(),
    itemBuilder: (context, index) {
      return Card(
        child: Center(
          child: Container(
            //height: 180,
            child: ListTile(
              /*leading: ImageIcon(
            AssetImage('icons/sprout.png'),
          ),*/
              title: Text(
                "Rack #${farmRacks[index]}",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Container(
                child: FutureBuilder<RackDetails>(
                  future: DataSource()
                      .fetchRackDetails(farmerID, farmID, farmRacks[index])
                      .then((result) {
                    return result;
                  }),
                  //fetchRackDetails(farmerID, farmID, farmRacks[index]),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      List<SensorInfo> sensorInfo = snapshot.data.sensorInfo;
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            height: 10,
                          ),
                          Container(
                            height: 120,
                            child: Scrollbar(
                              child: ListView.builder(
                                itemCount: sensorInfo.length,
                                //physics: NeverScrollableScrollPhysics(),
                                itemExtent: 30,
                                itemBuilder: (context, index) {
                                  return Text(
                                      "${sensorInfo[index].sensorGroup} ${sensorInfo[index].sensorName}: ${sensorInfo[index].sensorMType}");
                                },
                              ),
                            ),
                          ),
                          RaisedButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => RackWidget(
                                    "Rack #${farmRacks[index]}",
                                    farmInfo,
                                    snapshot.data,
                                  ),
                                ),
                              );
                            },
                            child: const Text(
                              "Enter Farm",
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                            color: Colors.teal,
                          ),
                        ],
                      );
                    } else if (snapshot.hasError) {
                      return Text("Error: ${snapshot.error}");
                    }

                    return Center(
                      child: Container(
                        height: 20,
                        width: 20,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  },
                ),
              ),
              //trailing: Icon(Icons.keyboard_arrow_right),
              /*onTap: () {
              },*/
            ),
          ),
        ),
      );
    },
  );
}

class FarmWidget extends StatelessWidget {
  const FarmWidget(this.farmerID, this.farmerInfo, {Key key}) : super(key: key);

  final String farmerID;
  final Farm farmerInfo;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(farmerInfo.farmInfo.farmName),
        //backgroundColor: Colors.teal,
      ),
      body: Column(
        children: <Widget>[
          Card(
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image(
                  image: AssetImage("images/smartfarm_bg.jpg"),
                  //width: 150,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "\n  Location:             \n",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        farmerInfo.farmInfo.farmLocation,
                        overflow: TextOverflow.clip,
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "\n  Sensor Sets:      \n",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "${farmerInfo.farmNodecount}/${farmerInfo.farmNodecount}",
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ]),
                //Column(),
              ],
            ),
          ),
          /*RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text(
                  "Exit Farm",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  ),
                ),
                color: Colors.teal,
              ),*/
          Container(
            constraints: BoxConstraints.expand(
              height: Theme.of(context).textTheme.headline4.fontSize + 5.0,
            ),
            padding: const EdgeInsets.all(8.0),
            alignment: Alignment.centerLeft,
            child: Text(
              "Farm Racks",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: FutureBuilder<FarmDetails>(
              future: DataSource()
                  .fetchFarmDetails(farmerID, farmerInfo.farmId)
                  .then((result) {
                return result;
              }),
              //fetchFarmDetails(farmerID, farmerInfo.farmId),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<String> rackList = [];
                  for (var farmNode in snapshot.data.farmNodes) {
                    if (!rackList.contains(farmNode.rackId)) {
                      rackList.add(farmNode.rackId);
                    }
                  }
                  rackList.sort();
                  return showRackList(
                      rackList, farmerID, farmerInfo.farmId, farmerInfo);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }

                return Center(
                  child: Container(
                    height: 20,
                    width: 20,
                    child: CircularProgressIndicator(),
                  ),
                );
              },
            ),
          ),
          Container(
            constraints: BoxConstraints.expand(
              height: Theme.of(context).textTheme.headline4.fontSize + 5.0,
            ),
            padding: const EdgeInsets.all(8.0),
            alignment: Alignment.centerLeft,
            child: Text(
              "Farm Controls",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            height: 70,
            child: Card(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    "AC:",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    width: 80,
                    height: 30,
                    child: TextField(
                      obscureText: false,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "min",
                      ),
                    ),
                  ),
                  Text(
                    "-",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    width: 80,
                    height: 30,
                    child: TextField(
                      obscureText: false,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'max',
                      ),
                    ),
                  ),
                  Container(
                    height: 30,
                    child: FlatButton(
                      onPressed: () {},
                      child: const Text(
                        'Set',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                      color: Colors.teal,
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            height: 5,
          ),
        ],
      ),
      /*floatingActionButton: FloatingActionButton(
        onPressed: () {
          /*final snackBar = SnackBar(
            content: Text("Button Clicked."),
          );
          Scaffold.of(context).showSnackBar(snackBar);
          showBottomSheet(
            context: context,
            builder: (context) => Container(
              color: Colors.red,
              height: 95,
            ),
          );*/
        },
      ),*/
    );
  }
}
