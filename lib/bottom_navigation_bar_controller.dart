import 'package:ProjectSPICE/widget_dashboard.dart';
import 'package:ProjectSPICE/widget_farms.dart';
import 'package:ProjectSPICE/widget_settings.dart';
import 'package:flutter/material.dart';

class BottomNavigationBarController extends StatefulWidget {
  BottomNavigationBarController({Key key, this.title}) : super(key: key);

  final String title;

  @override
  MyHomePageWidgetState createState() => MyHomePageWidgetState();
}

class MyHomePageWidgetState extends State<BottomNavigationBarController> {
  int selectedIndex = 0;
  final PageStorageBucket pageStorageBucket = PageStorageBucket();
  static const TextStyle textStyle =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  final List<Widget> pageWidgets = <Widget>[
    DashboardWidget(
      key: PageStorageKey("DashboardTab"),
    ),
    FarmsWidget(
      "testadmin",
      key: PageStorageKey("FarmsTab"),
    ),
    SettingsWidget(
      key: PageStorageKey("SettingsTab"),
    ),
  ];

  Widget _bottomNavigationBar() => BottomNavigationBar(
        onTap: onItemTapped,
        currentIndex: selectedIndex,
        //selectedItemColor: Colors.amber[800],
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Dashboard'),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("icons/sprout.png"),
            ),
            title: Text('Farms'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
          ),
        ],
      );

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        //backgroundColor: Colors.teal,
      ),
      body: PageStorage(
        child: pageWidgets[selectedIndex],
        bucket: pageStorageBucket,
      ),
      bottomNavigationBar: _bottomNavigationBar(),
      /*floatingActionButton: FloatingActionButton(
        //onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),*/ // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
