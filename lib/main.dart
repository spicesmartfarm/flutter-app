import 'package:flutter/material.dart';
import 'widget_login_class.dart';

//The main function is the starting point for all our Flutter Apps
void main() => runApp(MyApp());

// Main Application Widget
class MyApp extends StatelessWidget {
  static const String _title = "Project SPICE";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: LoginScreen(),
    );
  }
}
