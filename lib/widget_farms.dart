import 'package:ProjectSPICE/models/model_farms.dart';
import 'package:ProjectSPICE/my_classes/datasource.dart';
import 'package:ProjectSPICE/widget_farm.dart';

import 'package:flutter/material.dart';

ListView showFarmList(String farmerID, Farms farms) {
  return ListView.builder(
    itemCount: farms.farmCount,
    itemBuilder: (context, index) {
      return Card(
        child: Container(
          child: Center(
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('icons/sprout.png'),
              ),
              title: Text(
                "${farms.farmList[index].farmInfo.farmName}",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                  "100% Functional \n${farms.farmList[index].farmInfo.farmLocation}"),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          FarmWidget(farmerID, farms.farmList[index])),
                );
              },
            ),
          ),
        ),
      );
    },
  );
}

class FarmsWidget extends StatelessWidget {
  const FarmsWidget(this.farmerID, {Key key}) : super(key: key);

  final String farmerID;

  /*Future<Album> futureAlbum;
  @override
  void initState() {
    super.initState();
    futureAlbum = fetchAlbum();
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            constraints: BoxConstraints.expand(
              height: Theme.of(context).textTheme.headline4.fontSize + 5.0,
            ),
            padding: const EdgeInsets.all(8.0),
            alignment: Alignment.centerLeft,
            child: Text(
              "List of Farms",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: FutureBuilder<Farms>(
              future: DataSource().fetchFarms(farmerID).then((result) {
                return result;
              }),
              //fetchFarms(farmerID),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return showFarmList(farmerID, snapshot.data);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                return Center(
                  child: Container(
                    height: 20,
                    width: 20,
                    child: CircularProgressIndicator(),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
