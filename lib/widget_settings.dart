import 'package:flutter/material.dart';

class SettingsWidget extends StatelessWidget {
  const SettingsWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: RaisedButton(
              onPressed: () {
                final snackBar = SnackBar(
                  content: Text("Button Clicked."),
                );
                Scaffold.of(context).showSnackBar(snackBar);
              },
              child: const Text(
                'Sample Button',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
              color: Colors.teal,
            ),
          ),
        ],
      ),
    );
  }
}
